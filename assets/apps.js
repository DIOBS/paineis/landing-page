const app = Vue.createApp({
    data() {
        return {
            paineis: [
                {
                    name: "Taxa de natalidade, fecundidade, mortalidade no município de Fortaleza",
                    url: "https://observatoriodefortaleza.fortaleza.ce.gov.br/dados/painel/comparativo_bairros/",
                    image: "assets/img/comparativo_bairros.png",
                    description: "Comparativo das taxas de natalidade, fecundidade, mortalidade entre os bairros do município de Fortaleza",
                    status: "desenvolvimento"
                },
                {
                    name: "Escolas de Educação Básica no Ceará",
                    url: "https://observatoriodefortaleza.fortaleza.ce.gov.br/dados/painel/escolas_educacao_basica/",
                    image: "assets/img/escolas_ceara.png",
                    description: "Informações georreferenciadas das escolas do Ceará",
                    status: "desenvolvimento"
                },
                {
                    name: "Atlas da Economia de Fortaleza",
                    url: "https://observatoriodefortaleza.fortaleza.ce.gov.br/dados/painel/previa_mercado_de_trabalho/",
                    image: "assets/img/atlas_economia.png",
                    description: "Panorama econômico do município de Fortaleza",
                    status: "desenvolvimento"
                },
                {
                    name: "Atlas Capital Humano de Fortaleza",
                    url: "https://observatoriodefortaleza.fortaleza.ce.gov.br/dados/painel/atlas_capital_humano/",
                    image: "assets/img/atlas_capital_humano.png",
                    description: "Panorama do Capital Humano no município de Fortaleza",
                    status: "entregue"
                }
	        ]
        }
    }
}).mount('#app')
